package main

import (
	"log"
	"math/rand"
	"os"
	"strconv"
	"time"

	"go.uber.org/zap"
)

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func printLog(logger *zap.Logger, length int) {
	for i := 0; i < length; i++ {
		s := randSeq(rand.Intn(10))
		for j := 0; j < rand.Intn(20+5); j++ {
			s = s + " " + randSeq(rand.Intn(10)+5)
		}
		levelProbability := rand.Intn(10)
		if levelProbability > 8 {
			logger.Error(s)
		} else if levelProbability > 6 {
			logger.Warn(s)
		} else {
			logger.Info(s)
		}
	}
}

func Repeat(count int, sleep int) {
	rand.Seed(time.Now().UnixNano())

	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal(err)
	}

	// sugar := logger.Named("hehe")
	defer logger.Sync()

	if count <= 0 {
		count = 10
	}
	for {
		printLog(logger, count)
		time.Sleep(time.Duration(sleep) * time.Second)
	}
}

func main() {
	runCountEnv := os.Getenv("RUN_COUNT")
	sleepIntervalEnv := os.Getenv("SLEEP_INTERVAL")

	runCount := 99
	if runCountEnv != "" {
		runCount, _ = strconv.Atoi(runCountEnv)
	}

	sleepInterval := 60
	if sleepIntervalEnv != "" {
		sleepInterval, _ = strconv.Atoi(sleepIntervalEnv)
	}

	Repeat(runCount, sleepInterval)
}
